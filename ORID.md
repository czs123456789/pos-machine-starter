Objective: I learned what tasking and context map are. At the same time, our team conducted relevant exercises to familiarize ourselves with the relevant knowledge.

Reflective:  Fulfilled.

Interpretive: Tasking is a method of decomposing tasks. Complete tasks by breaking them down into small modules. The context map is a graphical explanation of tasking.

Decisional:  The combination of tasking and context map can make the project process more organized and help the completion of the project.