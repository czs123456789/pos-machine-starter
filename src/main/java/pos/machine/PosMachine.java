package pos.machine;

import java.util.ArrayList;
import java.util.List;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        //call decodeToItems() to obtain receiptItems
        List<ReceiptItem> receiptItems = decodeToItems(barcodes);
        Receipt receipt = calculateCost(receiptItems);
        String result = renderReceipt(receipt);
        return result;
    }

    /*
    description: obtain the data,which format as ReceiptItem.Class.The subTotal defaults to zero.
    return: List<ReceiptItem>
     */
    public List<ReceiptItem> decodeToItems(List<String> barcodes) {
        List<Item> items = ItemsLoader.loadAllItems();

        List<ReceiptItem> receiptItems = new ArrayList<>();

        items.stream().forEach(item -> {
            //int num may have unknown issues with stream()
            int num = 0;
            //Count the number of each barcode
            for (String barcode : barcodes) {
                if (barcode.equals(item.getBarcode())) {
                    num++;
                }
            }
            receiptItems.add(new ReceiptItem(item.getName(),num,item.getPrice(),0));
        });

//        for (Item item : items) {
//            int num = 0;
//            //Count the number of each barcode
//            for (String barcode : barcodes) {
//                if (barcode.equals(item.getBarcode())) {
//                    num++;
//                }
//            }
//            receiptItems.add(new ReceiptItem(item.getName(),num,item.getPrice(),0));
//        }

        return receiptItems;
    }

    public List<ReceiptItem> calculateItemsCost(List<ReceiptItem> paraReceiptItems) {
        List<ReceiptItem> receiptItems = new ArrayList<>();
        paraReceiptItems.stream().forEach(receiptItem -> {receiptItem.setSubTotal(receiptItem.getQuantity()*receiptItem.getUnitPrice());
            receiptItems.add(receiptItem);});

//        for (ReceiptItem receiptItem : paraReceiptItems) {
//            receiptItem.setSubTotal(receiptItem.getQuantity()*receiptItem.getUnitPrice());
//            receiptItems.add(receiptItem);
//        }
        return receiptItems;
    }

    public Integer calculateTotalPrice(List<ReceiptItem> receiptItems) {
        int total = 0;

        for (ReceiptItem receiptItem : receiptItems) {
            total += receiptItem.getSubTotal();
        }

        return total;
    }

    public Receipt calculateCost(List<ReceiptItem> ParaReceiptItems) {
        List<ReceiptItem> receiptItems = calculateItemsCost(ParaReceiptItems);
        int total = calculateTotalPrice(receiptItems);
        Receipt receipt = new Receipt();
        receipt.setTotalPrice(total);
        receipt.setReceiptItems(receiptItems);
        return receipt;
    }

    public String generateItemsReceipt(Receipt receipt) {
        List<ReceiptItem> receiptItems = receipt.getReceiptItems();
        StringBuilder stringBuilder = new StringBuilder();
        receiptItems.stream().forEach(receiptItem -> {stringBuilder.append("Name: "+receiptItem.getName()+", Quantity: "+receiptItem.getQuantity()
                +", Unit price: "+receiptItem.getUnitPrice()+" (yuan), Subtotal: " +receiptItem.getSubTotal() +" (yuan)\n");});
//        for (ReceiptItem receiptItem : receiptItems) {
//            stringBuilder.append("Name: "+receiptItem.getName()+", Quantity: "+receiptItem.getQuantity()+", Unit price: "+receiptItem.getUnitPrice()+" (yuan), Subtotal: " +receiptItem.getSubTotal() +" (yuan)\n");
//        }
        return stringBuilder.toString();
    }

    public String generateReceipt(String ItemsReceipt,int total) {
        String receipt = "***<store earning no money>Receipt***\n" + ItemsReceipt +
                "----------------------\n" +
                "Total: "+total+" (yuan)\n" +
                "**********************";
        return receipt;
    }

    public String renderReceipt(Receipt receipt) {
        String ItemsReceipt = generateItemsReceipt(receipt);
        int total = receipt.getTotalPrice();
        String result = generateReceipt(ItemsReceipt,total);
        return result;
    }
}
